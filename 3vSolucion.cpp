#include <iostream>
#include <cmath>
#include <complex>
#include <fftw3.h>
using namespace std;

typedef complex<double> dcomp;

const double TMAX = 32; // el tiempo va entre 0 y 64 
const int sizePulso = 1024; 
const double deltaT = TMAX/sizePulso; // 0.64
const double deltaW = ((M_PI)/TMAX)/sizePulso;
const double size = 200;
int main(void){
  // variable complejas que se necesitan
  dcomp ii;
  dcomp Dispersion[sizePulso];
  dcomp N; 
  ii = -1; ii = sqrt(ii); /// unidad imaginaria 
   

  // Parametros de la ecuacion 

  double Gamma = 1.4 ; // parametro  no-lineal 
  double Beta2 = 20; // parametro de dispersion
  double alpha = 0;
  // Parametros tipo de pulso inicial:
  int m = 0; // cero para soliton 1 para super-gaussiano 
  double chirp = 0.0005; // entre mayor sea este parametro mas estrecho esta el pulso


// parametros de longitud
  double L = 10*size; // longitud de la fibra ( unidades de Ld, Longitud de dispesion)
  double nPasos = 1000*size ; // numero de pasos en los que se va a dividir la fibra.
  double h = L/nPasos; // h 

  
  // Variables que contendran los pulsos a los cuales se requiere aplicar fourier.
  //
  // el arreglo in va a entrar en el algoritmo fftw y el resultado se obtiene en out
  fftw_complex in[sizePulso], out[sizePulso], in2[sizePulso] ; 
  fftw_plan p, q;
  
  if( m == 0){ // llenar el pulso inicial con un soliton
    double t; ; // el tiempo inicial es de -64 y va aumentando
    int i = 0;
    for( t = -TMAX; t < TMAX ; t += 2*deltaT){
      
      in[i][0] = ( 1/cosh(t) )*(exp(-0.5*chirp*t*t) ) ;// parte real
      //cout << i << " " << pow(in[i][0],2) << endl; 
      in[i][1] = 0;
      i+=1;
    }
   
    /*for(int i = 0; i< 2*sizePulso ; i++){
      cout << t << " " << in[i][0] << endl; 
      t+= deltaT;
      }*/
	  
  }else {
    // gaussiano
    double t=0 ;
    int i = 0;
    for(t = -TMAX; t < TMAX ; t += 2*deltaT){

      in[i][0] = (1/(chirp*sqrt(2*M_PI )) )*exp(-0.5*((t*t)/(chirp*chirp) ) ) ;
      in[i][1] = 0;
      //cout << i << " " << pow(in[i][0] , 2) << endl;
      i+=1;
    }
  
  }
  // Calculamos el arreglo de los desfasces debido a la dispersion:
  double omega = -(M_PI/TMAX );
  int i = 0;
  // 
  for(omega = -(M_PI)/TMAX ; omega < (M_PI)/TMAX ; omega+= 2*deltaW){
    Dispersion[i] = exp(-0.5*h*ii*omega*omega*Beta2);
    //cout << omega << " " << real(Dispersion[i]) << endl;
    i+=1;
  }

  // AQUI EMPIEZA EL BUCLE 
  /// 1. calcular el factor no-lineal  
  /// 1.a eso es exp(hN) A(z,t);  N = iGamma*A**2


  for( int k = 0; k < nPasos; k++){

  for(int i = 0; i < sizePulso ; i++){
    double A2 = 0; 
    A2 = pow(in[i][0],2) + pow(in[i][1] , 2 ) ; // amplitud al cuadrado
    N = exp(ii*Gamma*A2*h*0.5);
    // cout << real(N) << endl;
    double Nr, Ni, Ar, Ai;
    Nr = real(N); Ni = imag(N);
    Ar = in[i][0] ; Ai = in[i][1];
    in[i][0] = (Ar*Nr - Ai*Ni);
    in[i][1] = (Ai*Nr + Ar*Ni ); 
    // producto exp(hN)*A(z,t)
    //cout << i << " " << in[i][0] << endl;
    
}
  p = fftw_plan_dft_1d(sizePulso, in, out , FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(p);
  fftw_destroy_plan(p);
  // ahora out contiene la transformada en el dominio de la frecuencia. 
  // F[ exp(hN)A(Z,t)  ] lo multiplicamos punto por punto con las dispersioones

  for( int i = 0; i < sizePulso ; i++){
    double Dr, Di, Fr, Fi;
    Dr = real(Dispersion[i]); Di = imag(Dispersion[i]);
    Fr = out[i][0] ; Fi = out[i][1];
    
    out[i][0] = (Dr*Fr-Di*Fi);
    out[i][1] = (Dr*Fi + Di*Fr );
    // el producto
    
  }

  // paso final calcular la transformada de fourier inversa! 

  q = fftw_plan_dft_1d(sizePulso, out, in2, FFTW_BACKWARD , FFTW_ESTIMATE);
  fftw_execute(q);
  fftw_destroy_plan(q);
  
  for(int i=0 ; i <sizePulso; i++ ){
    in[i][0] = in2[i][0]/(sizePulso);
    //cout << i <<" " << in[i][0] << endl;
    in[i][1] = in2[i][1]/(sizePulso);
    in2[i][0] = 0; in2[i][1] = 0;
  }
  
  }

  for(int i = 0; i < sizePulso ; i++){
    cout << i <<" " <<  pow(in[i][1],2) + pow(in[i][0],2) << endl;
  }
}
